import unittest
import log_analyzer as la
# import homework1.log_analyzer as la
import datetime

class LogCalculateTest(unittest.TestCase):

    def test_logfile_name(self):
        log_format, date = la.processing_log_filename('nginx-access-ui.log-20170520')
        self.assertTrue(log_format == 'log')
        self.assertTrue(date == datetime.date(year=2017, month=5, day=20))

        log_format, date = la.processing_log_filename('nginx-access-ui.log-20170520.gz')
        self.assertTrue(log_format == 'gz')
        self.assertTrue(date == datetime.date(year=2017, month=5, day=20))

        log_format, date = la.processing_log_filename('nginx-access-backend.log-20170520.gz')
        self.assertTrue(log_format == '')
        self.assertTrue(date == '')

        log_format, date = la.processing_log_filename('nginx-access-backend.log-20170520.bz')
        self.assertTrue(log_format == '')
        self.assertTrue(date == '')

    def test_log_calculate(self):
        log_filename='nginx-access-ui.log-20170520'
        list_log = la.parse_logfile(log_filename = log_filename)
        list_request_stats = la.calculate_stats(list_log)[:10]
        self.assertTrue(int(list_request_stats[0]['time_avg']) == 8)
        self.assertTrue(int(list_request_stats[-1]['time_avg']) == 2)

if __name__ == '__main__':
    unittest.main()