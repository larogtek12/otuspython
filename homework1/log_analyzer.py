#!/usr/bin/env python
# -*- coding: utf-8 -*-
import gzip
import os
import sys
import re
import csv
from itertools import groupby
import statistics
import datetime
import json 
from string import Template
import argparse
import gzip

import logging


# log_format ui_short '$remote_addr  $remote_user $http_x_real_ip [$time_local] "$request" '
#                     '$status $body_bytes_sent "$http_referer" '
#                     '"$http_user_agent" "$http_x_forwarded_for" "$http_X_REQUEST_ID" "$http_X_RB_USER" '
#                     '$request_time';


config = {
    "REPORT_SIZE": 1000,
    "REPORT_DIR": "./reports/",
    "LOG_DIR": "./log/"
}


def read_logfile(log_filename:str, open_func) -> list:
    with open_func(log_filename, mode='rt') as nginxLogFile:
        logStringsIterable = csv.reader(nginxLogFile, delimiter=" ", quotechar='"', lineterminator="\n")
        for logString in logStringsIterable:
            yield logString
            

def parse_logfile(log_filename):
    data = []
    open_func = gzip.open if log_filename.endswith(".gz") else open
    parse_logfile_generator = read_logfile(log_filename, open_func)
    errors_count = 0
    for i, logString in enumerate(parse_logfile_generator):
        try:
            row = {}
            row['remote_addr'] = logString[0]
            row['remote_user'] = logString[1]
            row['http_x_real_ip'] = logString[3]
            row['time_local'] = logString[4]+logString[5]
            row['request'] = logString[6].split(' ')[1] if ' ' in logString[6] else logString[6]
            row['status'] = logString[7]
            row['body_bytes_sent'] = logString[8]
            row['http_referer'] = logString[9]
            row['http_user_agent'] = logString[10]
            row['http_x_forwarded_for'] = logString[11]
            row['http_X_REQUEST_ID'] = logString[12]
            row['http_X_RB_USER'] = logString[13]
            row['request_time'] = logString[14]
            data.append(row)
        except:
            errors_count += 1
        
        if (errors_count != 0) and (errors_count/i > 0.1):
            logging.exception('So many errors')
            raise ValueError('So many errors')

    return data


def processing_log_filename(filename) -> tuple:

    filename_split = filename.split('.')
    
    if 'nginx-access-ui.log' not in filename:
        return '', ''

    if filename_split[-1] == 'gz':
        log_format = 'gz'
    elif 'log' in filename_split[-1]:
        log_format = 'log'
    else:
        return '', ''
        
    filename = filename[:-3] if log_format == 'gz' else filename
    date_str = filename[-8:]
    date = datetime.date(year=int(date_str[:4]), month=int(date_str[4:6]), day=int(date_str[6:]))

    return log_format, date
        

def get_last_logfile(path_dir:str) -> dict:
    last_date = datetime.date(2000, 1, 1)
    last_date_file = {}
    log_format = ''

    for file in os.listdir(path_dir):
         
        log_format, date = processing_log_filename(file)
        if log_format == '':
            continue

        if date > last_date:
            last_date = date
            last_date_file['format'] = log_format
            last_date_file['date'] = date
            last_date_file['file'] = file
            
    return last_date_file
        

def calculate_basic_stats(sorted_log_list, request, other_param) -> list:
    log_group_request = [[k, [float(req[1]) for req in list(g)] ]
           for k, g in groupby(
                 [[i[request], i[other_param]] for i in sorted_log_list],
                 lambda x: x[0])]

    request_stats = [{'url': x[0], 
                      'count': len(x[1]), 
                      'time_sum': sum(x[1]), 
                      'time_max': max(x[1]), 
                      'time_median': statistics.median(x[1])} 
                        for x in log_group_request]

    return request_stats


def calculate_stats(log_list:list) -> list:
    sorted_log_list = sorted(log_list, key=lambda x: x['request'])
    count_requests = calculate_basic_stats(sorted_log_list, 'request', 'request_time')

    sum_count = sum([x['count'] for x in count_requests])
    sum_time = sum([x['time_sum'] for x in count_requests])

    list_request_stats = [{ 'url': x['url'], 
                            'count': round(x['count'], 3),
                            'time_sum': round(x['time_sum'], 3),
                            'time_max': round(x['time_max'], 3),
                            'time_median': round(x['time_median'], 3),
                            'perc': round((x['count']/sum_count)*100 , 3),
                            'time_perc': round((x['time_sum']/sum_time)*100, 3),
                            'time_avg': round(x['time_sum']/x['count'], 3),
                        } for x in count_requests]
    list_request_stats = sorted(list_request_stats, key=lambda x: x['time_sum'], reverse=True)

    return list_request_stats


def create_report(template_report_file, list_request_stats, output_report_filename):
    with open(template_report_file) as f:
        report = f.readlines()

    json_requests = json.dumps(list_request_stats)
    report_with_json = [Template(x).safe_substitute({'table_json': json_requests}) for x in report ]

    with open(output_report_filename, mode='w') as f:
        f.writelines(report_with_json)


def main(config):
    logging.info('Start')

    last_date_file = get_last_logfile(config['LOG_DIR'])
    if not last_date_file:
        logging.info('End')
        return

    report_filename = f'report-{last_date_file["date"].strftime("%Y.%m.%d")}.html'
    output_report_filename = os.path.join(os.path.normpath(config["REPORT_DIR"]), report_filename)
    if os.path.exists(output_report_filename):
        logging.info('End')
        return
        
    list_log = parse_logfile(log_filename = config['LOG_DIR'] + last_date_file['file'])
    if not list_log:
        logging.info('End')
        return

    list_request_stats = calculate_stats(list_log)[:config['REPORT_SIZE']]
    template_report_file = './report.html'
    create_report(template_report_file, list_request_stats, output_report_filename)

    logging.info('End')


def arg_parse() -> dict:
    parser = argparse.ArgumentParser(description='script for parsing logs NGINX')
    parser.add_argument('--config', default='config.json', required=False, type=str)
    args = vars(parser.parse_args())
    return args


if __name__ == "__main__":
    args = arg_parse()
    with open(args['config'], "r") as read_file:
        arg_config = json.load(read_file)
    config.update(arg_config)
    log_file = config.get('LOGFILE', None)
    logging.basicConfig(filename=log_file, 
                        format='[%(asctime)s] %(levelname).1s %(message)s',
                        datefmt='%Y.%m.%d %H:%M:%S',
                        level=logging.DEBUG)

    try:
        main(config)
    except:
        logging.exception('So many errors ')
    
